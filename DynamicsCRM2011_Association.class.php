<?php

require_once 'DynamicsCRM2011.php';

class DynamicsCRM2011_Association extends DynamicsCRM2011_Entity {
	protected $entityLogicalName = 'association'; 
	protected $entityDisplayName = 'fullname';
	
	public function __toString() {
		$description = 'Association: '.$this->FullName.' <'.$this->ID.'>';
		return $description;
	}
}

?>