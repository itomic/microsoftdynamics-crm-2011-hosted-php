<?php

/* Include the class library for the Dynamics CRM 2011 Connector */
require_once 'DynamicsCRM2011.php';

//parse the config file 
$config = parse_ini_file("config.ini", true);


$loginUsername = $config["dynamics"]["loginUsername"];
$loginPassword = $config["dynamics"]["loginPassword"];
$discoveryServiceURL = $config["dynamics"]["organizationDiscoveryURL"];
$organizationUniqueName = $config["dynamics"]['organizationUniqueName'];

define('DEBUG', false);

if(php_sapi_name() != 'cli'){
    echo "<pre>";
}


echo date('Y-m-d H:i:s')."\tConnecting to the CRM... ";
$crmConnector = new DynamicsCRM2011_Connector($discoveryServiceURL, $organizationUniqueName, $loginUsername, $loginPassword, DEBUG);
echo 'Connected at '.date('Y-m-d H:i:s').PHP_EOL;



  $something = '<entityName>contact</entityName>
                   <entityId>f53d9f13-eb91-e311-8f59-005056ba549a</entityId>
                   <relationship>
                      <SchemaName>ahrc_contact_ahrc_interestareas</SchemaName>
                   </relationship>
                   <relatedEntities>
                      <EntityReference>
                         <Id>?</Id>
                         <LogicalName>?</LogicalName>
                         <Name>?</Name>
                      </EntityReference>
                   </relatedEntities>';

echo PHP_EOL;
echo date('Y-m-d H:i:s')."\t Associating data... ";

$entry = array(
  'entityName' => 'contact',
  'entityId' => 'f53d9f13-eb91-e311-8f59-005056ba549a',
);

$relatedEntities = array();
$relatedEntities[] = array(
  'Id' => 'Id-value',
  'LogicalName' => 'LogicalName-value',
  'Name' => 'Name-value',
);

$relationship = array(
  'SchemaName' => 'ahrc_contact_ahrc_interestareas',
);

$accountData = $crmConnector->associate($entry, $relationship, $relatedEntities);

echo 'Done'.PHP_EOL.PHP_EOL;

print_r($accountData);

$accountData = $crmConnector->disassociate($entry, $relationship, $relatedEntities);

echo 'Done'.PHP_EOL.PHP_EOL;

print_r($accountData);

exit('... END Associating data');



$accountQueryXML = <<<END
<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true" count="1">
  <entity name="contact">
    <all-attributes/>
    <filter type="and">
      <condition attribute="emailaddress1" operator="eq" value="nigel@itomic.com.au" />
    </filter>
  </entity>
</fetch>
END;

echo PHP_EOL;
echo date('Y-m-d H:i:s')."\tFetching Account data... ";
$accountData = $crmConnector->retrieveMultiple($accountQueryXML);
echo 'Done'.PHP_EOL.PHP_EOL;

foreach ($accountData->Entities as $account) {

    //echo "<pre>".print_r($account, true)."</pre>";
    
    /* Fetch fields individually */
    echo "\t".'Account <'.$account->firstname.'> has ID {'.$account->id.'}'.PHP_EOL;
    /* Use specific toString of Contact */

    $contactId = $account->id;

    
}




$accountQueryXML = <<<END
<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true"  count="1">
  <entity name="ahrc_interestarea">
    <all-attributes/>
  </entity>
</fetch>
END;

echo PHP_EOL;
echo date('Y-m-d H:i:s')."\tFetching ahrc_interestarea data... ";
$accountData = $crmConnector->retrieveMultiple($accountQueryXML);
echo 'Done'.PHP_EOL.PHP_EOL;




foreach ($accountData->Entities as $account) {

    //echo "<pre>".print_r($account, true)."</pre>";

    echo "ID : " . $account->ahrc_interestareaid .PHP_EOL.PHP_EOL;
        
}

echo 'End : ahrc_interestarea'.PHP_EOL.PHP_EOL;


        $something = '<entityName>contact</entityName>
                         <entityId>'.$contactId.'</entityId>
                         <relationship>
                            <PrimaryEntityRole>?</PrimaryEntityRole>
                            <SchemaName>ahrc_contact_ahrc_interestareas</SchemaName>
                         </relationship>
                         <relatedEntities>
                            <EntityReference>
                               <Id>?</Id>
                               <LogicalName>?</LogicalName>
                               <Name>?</Name>
                            </EntityReference>
                         </relatedEntities>';



        $something = '<entityName>contact</entityName>
                         <entityId>'.$contactId.'</entityId>
                         <relationship>
                            <SchemaName>ahrc_contact_ahrc_interestareas</SchemaName>
                         </relationship>
                         <relatedEntities>
                            <EntityReference>
                               <Id>?</Id>
                               <LogicalName>?</LogicalName>
                               <Name>?</Name>
                            </EntityReference>
                         </relatedEntities>';

echo PHP_EOL;
echo date('Y-m-d H:i:s')."\t Associating data... ";

$something = DynamicsCRM2011_Entity::fromLogicalName($crmConnector, 'association');

$accountData = $crmConnector->associate($something);
echo 'Done'.PHP_EOL.PHP_EOL;

print_r($accountData);

exit('asd');

$accountQueryXML = <<<END
<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">
  <entity name="account">
    <attribute name="name" />
    <attribute name="primarycontactid" />
    <attribute name="telephone1" />
    <attribute name="accountid" />
    <attribute name="ownerid" />
    <filter type="and">
      <condition attribute="ownerid" operator="eq" value="da1887f9-3d73-e311-aef6-005056ba549a" />
    </filter>
  </entity>
</fetch>
END;

echo PHP_EOL;
echo date('Y-m-d H:i:s')."\tFetching Account data... ";
$accountData = $crmConnector->retrieveMultiple($accountQueryXML);
echo 'Done'.PHP_EOL.PHP_EOL;




foreach ($accountData->Entities as $account) {

    //echo "<pre>".print_r($account, true)."</pre>";
    
    /* Fetch fields individually */
    echo "\t".'Account <'.$account->name.'> has ID {'.$account->accountid.'}'.PHP_EOL;
    /* Use specific toString of Contact */

    $contactId = $account->primarycontactid->id;
    echo "\t\t".'Primary Contact is: '.$account->PrimaryContactId.PHP_EOL;
    echo "\t\t".'Primary Contact id is : '.$account->primarycontactid->id.PHP_EOL;
    /* Use automatic toString of SystemUser */
    echo "\t\t".'Owner is: '.$account->OwnerId.PHP_EOL;
    
}

$accountQueryXML = <<<END
<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true"  count="1">
  <entity name="contact">
    <all-attributes/>
  </entity>
</fetch>
END;

echo PHP_EOL;
echo date('Y-m-d H:i:s')."\tFetching Account data... ";
$accountData = $crmConnector->retrieveMultiple($accountQueryXML);
echo 'Done'.PHP_EOL.PHP_EOL;

foreach ($accountData->Entities as $account) {

    //echo "<pre>".print_r($account, true)."</pre>";
    
    /* Fetch fields individually */
    echo "\t".'Account <'.$account->name.'> has ID {'.$account->accountid.'}'.PHP_EOL;
    /* Use specific toString of Contact */

    $contactId = $account->primarycontactid->id;
    echo "\t\t".'Primary Contact is: '.$account->PrimaryContactId.PHP_EOL;
    echo "\t\t".'Primary Contact id is : '.$account->primarycontactid->id.PHP_EOL;
    /* Use automatic toString of SystemUser */
    echo "\t\t".'Owner is: '.$account->OwnerId.PHP_EOL;
    
}



echo PHP_EOL;
echo date('Y-m-d H:i:s')."\tFetching Contact data... ";
$contactData = $crmConnector->retrieveMultiple($accountQueryXML);
echo 'Done'.PHP_EOL.PHP_EOL;



$dataQueryXML = <<<END
<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true"  count="1">
  <entity name="new_ahrc_interestareas">
    <all-attributes/>
  </entity>
</fetch>
END;

echo PHP_EOL;
echo date('Y-m-d H:i:s')."\tFetching Account data... ";
$data = $crmConnector->retrieveMultiple($dataQueryXML);
echo 'Done'.PHP_EOL.PHP_EOL;

foreach ($data->Entities as $account) {
    echo "<pre>".print_r($account, true)."</pre>";
}

exit('END');


$accountQueryXML = <<<END
<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">
  <entity name="contact">
    <attribute name="firstname" />
    <attribute name="lastname" />
    <attribute name="emailaddress1" />
    <attribute name="contactid" />
    <attribute name="accountid" />
    <filter type="and">
      <condition attribute="emailaddress1" operator="eq" value="nigel@itomic.com.au" />
    </filter>
  </entity>
</fetch>
END;

$accountQueryXML = <<<END
<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">
  <entity name="contact">
    <all-attributes/>
    <filter type="and">
      <condition attribute="emailaddress1" operator="eq" value="nigel@itomic.com.au" />
    </filter>
  </entity>
</fetch>
END;

$accountQueryXML = <<<END
<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">
  <entity name="contact">
    <all-attributes/>
    <filter type="and">
      <condition attribute="contactid" operator="eq" value="$contactId" />
    </filter>
  </entity>
</fetch>
END;

$accountQueryXML = <<<END
<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true"  count="1">
  <entity name="contact">
    <all-attributes/>
  </entity>
</fetch>
END;


echo PHP_EOL;
echo date('Y-m-d H:i:s')."\tFetching Contact data... ";
$contactData = $crmConnector->retrieveMultiple($accountQueryXML);
echo 'Done'.PHP_EOL.PHP_EOL;



foreach ($contactData->Entities as $contact) {

    //echo "<pre>".print_r($contact, true)."</pre>";
    echo "\t\t".'Contact Fields : ' . print_r(array_keys($contact->properties), true).PHP_EOL;

    echo "\t\t".'Contact Fields : ' .print_r($contact->properties, true).PHP_EOL;


    /* Fetch fields individually */
    echo "\t".'Contact <'.$contact->firstname.' '.$contact->lastname.'> has ID {'.$contact->contactid.'}'.PHP_EOL;
    /* Use specific toString of Contact */
    $parentcustomerid = $contact->parentcustomerid;
    echo "\t\t".'Parent account id is: '.$contact->parentcustomerid.PHP_EOL;
    /* Use automatic toString of SystemUser */
    echo "\t\t".'Owner is: '.$contact->OwnerId.PHP_EOL;

    echo "\t\t".'Email is: '.$contact->emailaddress1.PHP_EOL;
}

echo PHP_EOL;
echo date('Y-m-d H:i:s')."\tCreating new contact... " . PHP_EOL;
$contact = DynamicsCRM2011_Entity::fromLogicalName($crmConnector, 'contact');
$contact->firstname = "New Contact " . date('Y-m-d H:i:s');
$contact->lastname = date('Y-m-d H:i:s');
$contact->telephone1 = "123456";
//$account->primarycontactid = $parentcustomerid;
$created = $crmConnector->create($contact);
echo 'New contact with name:' . $contact->firstname . ' ' . $contact->lastname .PHP_EOL;
echo 'New contact with id:' . $created .PHP_EOL;


echo PHP_EOL;
echo date('Y-m-d H:i:s')."\tCreating new account with primary contact from above... " . PHP_EOL;
$account = DynamicsCRM2011_Entity::fromLogicalName($crmConnector, 'account');
$account->name = "New Account " . date('Y-m-d H:i:s');
$account->telephone1 = "123456";
$account->primarycontactid = $contact;
$created = $crmConnector->create($account);
echo 'New account with name:' . $account->name .PHP_EOL;
echo 'New account with id:' . $created .PHP_EOL;
//$contact->ID = $contactId;
//
if(php_sapi_name() != 'cli'){
    echo "</pre>";
}
